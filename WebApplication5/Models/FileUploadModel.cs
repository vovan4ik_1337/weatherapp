﻿namespace WebApplication5.Models
{
    public class FileUploadModel
    {
        public IEnumerable<IFormFile> MultipleFiles { get; set; }
    }
}
