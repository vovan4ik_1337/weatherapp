﻿using DomainModel.Models;

namespace WebApplication5.Models
{
    public class EntriesTableViewModel
    {
        public IEnumerable<WeatherEntry> WeatherEntries { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
