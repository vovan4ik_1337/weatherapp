﻿using DomainModel;
using DomainModel.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System.Diagnostics;
using WebApplication5.Models;

namespace WebApplication5.Controllers
{
    public class HomeController : Controller
    {
        private readonly WeatherAppContext _context;
        public const int DataFirstRow = 4;
        public const int NumberOfColumns = 12;
        public const int PageSize = 31;

        public HomeController(WeatherAppContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<IActionResult> EntriesTable(int? selectedYear, int? selectedMonth, int page = 1)
        {
            EntriesTableViewModel viewModel = new EntriesTableViewModel();

            var entries = _context.WeatherEntries.Select(x => new WeatherEntry()
            {
                WeatherEntryId = x.WeatherEntryId,
                DateTime = x.DateTime,
                AirTemperature = x.AirTemperature,
                RelativeHumidity = x.RelativeHumidity,
                DewPoint = x.DewPoint,
                AtmPressure = x.AtmPressure,
                WindDirection = x.WindDirection,
                WindSpeed = x.WindSpeed,
                CloudCover = x.CloudCover,
                CloudCoverLowBorder = x.CloudCoverLowBorder,
                HorizontalVisibility = x.HorizontalVisibility,
                WeatherEvent = x.WeatherEvent,
                CreatedDate = x.CreatedDate

            });

            if (selectedYear != null)
            {
                entries = entries.Where(x => x.DateTime.Date.Year == selectedYear);
            }

            if (selectedMonth != 0 && selectedMonth != null)
            {
                entries = entries.Where(x => x.DateTime.Date.Month == selectedMonth);
            }

            ViewData["selectedYear"] = selectedYear;
            ViewData["selectedMonth"] = selectedMonth;

            var count = await entries.CountAsync();

            PageViewModel pageViewModel = new PageViewModel(count, page, PageSize);

            viewModel.PageViewModel = pageViewModel;
            viewModel.WeatherEntries = await entries.Skip((page - 1) * PageSize).Take(PageSize).ToListAsync();

            return View(viewModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public async Task<ActionResult> UploadFile(FileUploadModel model)
        {
            if (model.MultipleFiles == null)
                return RedirectToAction("Index");

            foreach (var file in model.MultipleFiles)
            {
                if (file.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                    continue;

                IWorkbook workBook = new XSSFWorkbook(file.OpenReadStream());

                for (int i = 0; i < workBook.NumberOfSheets; i++)
                {
                    ISheet sheet = workBook.GetSheetAt(i);

                    for (int j = DataFirstRow; j < sheet.PhysicalNumberOfRows; j++)
                    {
                        IRow row = sheet.GetRow(j);

                        DateTime entryDate;
                        DateTime entryTime;

                        if (row.GetCell(0) == null || row.GetCell(1) == null ||
                            !DateTime.TryParse(row.GetCell(0).ToString(), out entryDate) ||
                            !DateTime.TryParse(row.GetCell(1).ToString(), out entryTime))
                            continue;

                        try
                        {
                            WeatherEntry newEntry = new WeatherEntry()
                            {
                                DateTime = entryDate.Add(entryTime.TimeOfDay),
                                AirTemperature = GetNumericValueFromTheFile(row, 2),
                                RelativeHumidity = GetNumericValueFromTheFile(row, 3),
                                DewPoint = GetNumericValueFromTheFile(row, 4),
                                AtmPressure = (int?)GetNumericValueFromTheFile(row, 5),
                                WindDirection = row.GetCell(6)?.ToString(),
                                WindSpeed = (int?)GetNumericValueFromTheFile(row, 7),
                                CloudCover = (int?)GetNumericValueFromTheFile(row, 8),
                                CloudCoverLowBorder = (int?)GetNumericValueFromTheFile(row, 9),
                                HorizontalVisibility = row.GetCell(10)?.ToString(),
                                WeatherEvent = row.GetCell(11)?.ToString(),
                                CreatedDate = DateTime.Now
                            };

                            await _context.AddAsync(newEntry);
                        }
                        catch (Exception ex)
                        {
                            continue;
                        }

                    }
                }
            }

            await _context.SaveChangesAsync();

            await RemoveEntriesExceptLatest();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Этот метод удаляет все записи с повторяющимися датами, кроме самых последних. Возможно есть более умный способ это сделать, но я не придумал
        /// </summary>
        /// <returns></returns>
        private async Task RemoveEntriesExceptLatest()
        {
            var entries = _context.WeatherEntries.FromSqlRaw(@"SELECT  
  we.WeatherEntryId, we.[DateTime], we.AirTemperature, we.RelativeHumidity, we.DewPoint, we.AtmPressure, we.WindDirection, we.WindSpeed, we.CloudCover, we.CloudCoverLowBorder, we.HorizontalVisibility, we.WeatherEvent, max(we.CreatedDate) AS CreatedDate
FROM WeatherEntries we
  WHERE we.CreatedDate = (SELECT TOP 1 MAX(we2.CreatedDate) FROM WeatherEntries we2 WHERE we.[DateTime] = we2.[DateTime])
  GROUP BY
we.WeatherEntryId, we.[DateTime], we.AirTemperature, we.RelativeHumidity, we.DewPoint, we.AtmPressure, we.WindDirection, we.WindSpeed, we.CloudCover, we.CloudCoverLowBorder, we.HorizontalVisibility, we.WeatherEvent").ToList();

            await _context.WeatherEntries.ExecuteDeleteAsync();

            entries.ForEach(x => x.WeatherEntryId = 0);

            await _context.AddRangeAsync(entries);

            await _context.SaveChangesAsync();
        }

        private static float? GetNumericValueFromTheFile(IRow row, int cell)
        {
            return !string.IsNullOrWhiteSpace(row.GetCell(cell)?.ToString()) ? (float?)row.GetCell(cell)?.NumericCellValue : null;
        }
    }
}