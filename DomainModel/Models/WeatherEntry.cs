﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainModel.Models
{
    public class WeatherEntry
    {
        [Key]
        public int WeatherEntryId { get; set; }
        /// <summary>
        /// Время и дата записи
        /// </summary>
        public DateTime DateTime { get; set; }
        /// <summary>
        /// T, температура воздуха, гр.Ц.
        /// </summary>
        public float? AirTemperature { get; set; }
        /// <summary>
        /// Относительная влажность воздуха, %
        /// </summary>
        public float? RelativeHumidity { get; set; }
        /// <summary>
        /// Td, точка росы, гр.Ц.
        /// </summary>
        public float? DewPoint { get; set; }
        /// <summary>
        /// Атмосферное давление, мм.рт.ст.
        /// </summary>
        public int? AtmPressure { get; set; }
        /// <summary>
        /// Направление ветра
        /// </summary>
        public string? WindDirection { get; set; }
        /// <summary>
        /// Скорость ветра, м/с
        /// </summary>
        public int? WindSpeed { get; set; }
        /// <summary>
        /// Облачность, %
        /// </summary>
        public int? CloudCover { get; set; }
        /// <summary>
        /// h, нижняя граница облачности, м.
        /// </summary>
        public int? CloudCoverLowBorder { get; set; }
        /// <summary>
        /// VV, горизонтальная видимость, км.
        /// </summary>
        public string? HorizontalVisibility { get; set; }
        /// <summary>
        /// Погодные явления
        /// </summary>
        public string? WeatherEvent { get; set; }
        /// <summary>
        /// Дата и время добавления записи
        /// </summary>
        public DateTime CreatedDate { get; set; }

    }
}
